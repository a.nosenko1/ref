#include <iostream>
#include <iomanip>

double ref(double x) {
    static const double
            t0 = 0.027304492336793458,
            t1 = 0.89177151647498938,
            t2 = 0.92990235438411353,
            t3 = -2.5823665218801173,
            t4 = 3.5372251948593050,
            t5 = -3.941416778983322,
            t6 = 3.4185654841031838,
            t7 = -2.1703181677408996,
            t8 = 0.99476337838824452,
            t9 = -0.32931186362940976,
            t10 = 0.078300656391483343,
            t11 = -0.013089641157595793,
            t12 = 0.0014654294315543009,
            t13 = -0.000098921091963349817,
            t14 = 3.0511893095010025e-6;
    if (x >= 1.5 && x <= 3.0){
        double x2 = x*x;
        double x3 = x2*x;
        double p1 = t0+x3*(t3+x3*(t6+x3*(t9 +x3*t12)));
        double p2 = t1+x3*(t4+x3*(t7+x3*(t10+x3*t13)));
        double p3 = t2+x3*(t5+x3*(t8+x3*(t11+x3*t14)));
        return x*p2 + x2*p3 + p1;
    }
    return 1;
}

int main() {
    double x;
    std::cin >> x;
    std::cout.setf(std::ios::fixed);
    std::cout << std::setprecision(30) << ref(x) << std::endl;
    return 0;
}
